package com.z.tds.utilities;

import com.z.tds.gui.Screen;
import com.z.tds.objects.Entity;

public class Camera {
	
	private int xOffset = 0;
	private int yOffset = 0;
	private Screen screen;

	public Camera(Screen screen) {
		this.screen = screen;
	}
	
	public void centerOnPlayer(Entity entity) {
		if(entity != null && screen != null) {
			xOffset = (int) (entity.getX() - screen.getScreenWidth() / 2 + 8);
			yOffset = (int) (entity.getY() - screen.getScreenHeight() / 2 + 8);		
		}							
	}
	
	public int getXOffset() {
		return xOffset;
	}
	
	public int getYOffset() {
		return yOffset;
	}


}
