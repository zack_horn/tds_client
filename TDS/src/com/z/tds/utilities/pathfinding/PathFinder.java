package com.z.tds.utilities.pathfinding;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.z.tds.worlds.tiles.MapTile;
import com.z.tds.worlds.tiles.MapTile.State;

public class PathFinder {
	 
    public static ArrayList<MapTile> findPath(MapTile startTile, MapTile destinationTile) {
    	System.out.println("starting search...");
        if (!destinationTile.isWalkable()) {
        	System.out.println("destination not walkable");
            return new ArrayList<>();
        } 
 
        ArrayList<MapTile> openList = new ArrayList<>(),
                closedList = new ArrayList<>();
        HashMap<MapTile, Heuristics> heuristicsHashMap = new HashMap<>();
 
        MapTile currentTile = startTile;
        MapTile nextTile = currentTile;
        Heuristics startHeuristics = new Heuristics(currentTile, destinationTile, null, 0f);
        heuristicsHashMap.put(currentTile, startHeuristics);
        closedList.add(currentTile);
       
        while (!currentTile.equals(destinationTile)) {
 
            for (MapTile neighbor : currentTile.getNeighbors()) {
            	//neighbor.setState(State.SEARCHED);
                if (!neighbor.isWalkable() || closedList.contains(neighbor))
                    continue;
 
                
                if (!openList.contains(neighbor)) {
                    openList.add(neighbor);
                    Heuristics heuristics = new Heuristics(neighbor, destinationTile,
                            currentTile, heuristicsHashMap.get(currentTile).getGCost());
                    heuristicsHashMap.put(neighbor, heuristics);
                } else {
                    Heuristics previousHeuristics = heuristicsHashMap.get(neighbor);
                    Heuristics currentHeuristics = new Heuristics(neighbor, destinationTile,
                            currentTile, heuristicsHashMap.get(currentTile).getGCost());
                    if (currentHeuristics.getGCost() < previousHeuristics.getGCost())
                        heuristicsHashMap.replace(neighbor, currentHeuristics);
                }
               // neighbor.setState(MapTile.State.SEARCHED);
            }
 
            for (MapTile tile : openList) {
                if (nextTile.equals(currentTile)) {
                    nextTile = tile;
                } else {
                    if (heuristicsHashMap.get(tile).getGCost() <= heuristicsHashMap.get(nextTile).getGCost())
                        nextTile = tile;
                }
            }
 
            if (currentTile.equals(nextTile))
                return new ArrayList<>();
 
            currentTile = nextTile;
            openList.remove(currentTile);
            closedList.add(currentTile);
        }
 
        ArrayList<MapTile> path = new ArrayList<>();
        while (!currentTile.equals(startTile)) {
            currentTile.setState(MapTile.State.PATHED);
            path.add(currentTile);
            Heuristics currentHeuristics = heuristicsHashMap.get(currentTile);
            currentTile = currentHeuristics.getParentTile();
        }
        for(MapTile tile: path) {
        	//tile.setState(State.SEARCHED);
        	tile.setColor(Color.RED);
        }
        Collections.reverse(path);
        System.out.println("Searched finished with " + path.size() + " amount of tiles in path..");
        return path;
    }
 
}
