package com.z.tds.utilities.pathfinding;

import com.z.tds.worlds.tiles.MapTile;

public class Heuristics {
	 
    private float hCost, gCost, parentMovementCost;
    private MapTile tile, destinationTile, parentTile;
 
    public Heuristics(MapTile tile, MapTile destinationTile, MapTile parentTile, float parentMovementCost) {
        this.tile = tile;
        this.destinationTile = destinationTile;
        this.parentTile = parentTile;
        this.parentMovementCost = parentMovementCost;
        this.hCost = calculateHCost();
        this.gCost = calculateGCost();
    }
 
    private float calculateHCost() {
        return getDistanceFrom(tile, destinationTile) * 10;
    }
 
    private float calculateGCost() {
        float movementCost = parentMovementCost;
        if (parentTile != null)
            if (isDiagonal(parentTile, tile))
                movementCost += 14f;
            else
                movementCost += 10f;
        else
            movementCost += 0f;
 
        return movementCost;
    }
 
    private static boolean isDiagonal(MapTile currentTile, MapTile destinationTile) {
        return Math.abs(currentTile.getX() - destinationTile.getX()) == 1 &&
                Math.abs(currentTile.getY() - destinationTile.getY()) == 1;
    }
 
    private static float getDistanceFrom(MapTile startTile, MapTile destinationTile) {
        int differenceInX = Math.abs(startTile.getX() - destinationTile.getX());
        int differenceInY = Math.abs(startTile.getY() - destinationTile.getY());
        return (float) differenceInX + differenceInY;
    }
 
    public float getGCost() {
        return gCost;
    }
 
    public float getFCost() {
        return hCost + gCost;
    }
 
    public MapTile getParentTile() {
        return parentTile;
    }
}
