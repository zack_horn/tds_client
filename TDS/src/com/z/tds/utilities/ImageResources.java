package com.z.tds.utilities;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

public class ImageResources {
	
	private BufferedImage basicMapSpriteSheet;
	
	public ImageResources() {
		initBasicMapSpriteSheet();
	}
	
	
	public BufferedImage getPreGameLobbyBackgroundGUI() {
		URL preGameLobbyBackgroundURL = this.getClass().getResource("/gui/preGameLobbyGUI.png");
		try {
			return ImageIO.read(preGameLobbyBackgroundURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public BufferedImage getBasicPlayerSprite() {
		URL basicPlayerSpriteURL = this.getClass().getResource("/playerSprites/hitman1_gun.png");
		try {
			return ImageIO.read(basicPlayerSpriteURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void initBasicMapSpriteSheet() {
		URL basicMapSpriteSheetURL = this.getClass().getResource("/tileSets/basicTileSheet.png");
		try {
			basicMapSpriteSheet = ImageIO.read(basicMapSpriteSheetURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public BufferedImage getRandomBasicGrassTileSprite() {
		Random random = new Random();
		int r = random.nextInt(2);
		if(r == 0) {
			return basicMapSpriteSheet.getSubimage(0, 0, 16, 16);
		}
		if(r == 1) {
			return basicMapSpriteSheet.getSubimage(16, 0, 16, 16);
		}
		if(r == 2) {
			return basicMapSpriteSheet.getSubimage(32, 0, 16, 16);
		}
		return null;
	}
	
	public BufferedImage getBasicRockTileSprite() {
		return basicMapSpriteSheet.getSubimage(0, 32, 16, 16);
	}
	
	public ArrayList<BufferedImage> getWaterTileAnimationSprites() {
		ArrayList<BufferedImage> sprites = new ArrayList<BufferedImage>();
		BufferedImage tileSpriteOne = basicMapSpriteSheet.getSubimage(0, 16, 16, 16);
		BufferedImage tileSpriteTwo = basicMapSpriteSheet.getSubimage(16, 16, 16, 16);
		BufferedImage tileSpriteThree = basicMapSpriteSheet.getSubimage(32, 16, 16, 16);
		sprites.add(tileSpriteOne);
		sprites.add(tileSpriteTwo);
		sprites.add(tileSpriteThree);
		return sprites;		
	}
	
	public BufferedImage getBasicButtonSpriteOpacityFull() {
		URL buttonOneURL = this.getClass().getResource("/gui/buttons/buttonOne_192x32_opacity100.png");
		try {
			return ImageIO.read(buttonOneURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public BufferedImage getBasicButtonSpriteOpacityHalf() {
		URL buttonOneURL = this.getClass().getResource("/gui/buttons/buttonOne_192x32_opacity50.png");
		try {
			return ImageIO.read(buttonOneURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public BufferedImage getCountryFlag(String abreviation) {
		URL flagURL = null;
		if(abreviation.contains("us")) {
			flagURL = this.getClass().getResource("/countryFlags/us.png");
		}
		if(abreviation.contains("bb")) {
			flagURL = this.getClass().getResource("/countryFlags/bb.png");
		}
		if(abreviation.contains("en")) {
			flagURL = this.getClass().getResource("/countryFlags/en.png");
		}
		if(abreviation.contains("ca")) {
			flagURL = this.getClass().getResource("/countryFlags/ca.png");
		}
		
		try {
			return ImageIO.read(flagURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
