package com.z.tds.protocol;

public class GunProtocol {
	public static int SNIPER_308_BULLET_SPEED = 25;
	public static int AK47_BULLET_SPEED = 10;
	public static int PISTOL_45_BULLET_SPEED = 7;
	public static int SHOTGUN_BIRDSHOT_BULLET_SPEED = 7;
	public static int PARTICAL_SPEED = 1;
	
	public static int PARTICLE_RANGE = 12;
	public static int SNIPER_308_BULLET_RANGE = 420;
	public static int AK47_BULLET_RANGE = 310;
	public static int SHOTGUN_BIRDSHOT_BULLET_RANGE = 175;
	public static int PISTOL_45_BULLET_RANGE = 280;
	
	public static String SNIPER_308_BULLET_TYPE = "SNIPER308";
	public static String AK47_BULLET_TYPE = "AK47";
	public static String PISTOL_45_BULLET_TYPE = "PISTOL45";
	public static String SHOTGUN_BIRDSHOT_BULLET_TYPE = "SHOTGUNBIRDSHOT";
	public static String PARTICLE_BULLET_TYPE = "PARTICLE";
	
	public static int PLAYER_FIST_WEAPON_ITEM_ID = 0;
	public static int SNIPER_308_WEAPON_ITEM_ID = 1;
	public static int AK47_WEAPON_ITEM_ID = 2;
	public static int PISTOL_45_WEAPON_ITEM_ID = 3;
	public static int SHOTGUN_WEAPON_ITEM_ID = 4;
	
	public static String SNIPER_308_WEAPON_DESCRIPTION = ".308 Hunting Rifle";
	public static String PISTOL_45_WEAPON_DESCRIPTION = ".45 Pistol";
	public static String AK47_WEAPON_DESCRIPTION = "AK 47";
	public static String PLAYER_FIST_DESCRIPTION = "Fists";
	
	public static int AK47_DAMAGE_VALUE = 22;
	public static int SNIPER_308_DAMAGE_VALUE = 66;
	public static int PISTOL_45_DAMAGE_VALUE = 17;
	
	public static int AK47_WEAPON_FIRE_RATE = (int) (TimeProtocol.SECOND / 10);
	public static int SNIPER_308_FIRE_RATE = (int) (TimeProtocol.SECOND * 2);
	public static int PISTOL_45_FIRE_RATE = (int) (TimeProtocol.SECOND / 6);
	
	public static int AK47_MAX_CLIP_SIZE = 30;
	public static int SNIPER_308_MAX_CLIP_SIZE = 5;
	public static int PISTOL_45_MAX_CLIP_SIZE = 15;
}
