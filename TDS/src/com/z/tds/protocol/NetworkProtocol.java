package com.z.tds.protocol;

public class NetworkProtocol {
	
	
	//IDENTIFIERS////////////////////////////////////////
	
	public static String SQL_UPDATE_IDENTIFIER = "";	
	public static String FLOAT_SPLITTER_IDENTIFIER = "^&^";
	/////////////////////////////////////////////////////
	
	//SERVER COMMANDS////////////////////////////////////
	public static String END_MAIN_PROTOCOL_IDENTIFIER = "~_";
	public static String END_SUB_PROTOCOL_IDENTIFIER = "~-";
	public static String END_USERNAME_IDENTIFIER = "^#^";
	
	
	
	//////////////////MAIN PROTOCOL
	
						///LOGIN
	//////////////////////////////////////////////////////
	public static String LOGIN_PACKET_IDENTIFIER = "lpi";
					////login
	public static String LOGIN_SUCCESFUL = "^l*^";
	public static String LOGIN_FAILED = "^f*^";
	public static String LOGIN_ATTEMPT = "^la^";
	//////////////////////////////////////////////////////
	
	
						///PLAYER UPDATE
	//////////////////////////////////////////////////////
	public static String PLAYER_UPDATE_PACKET_IDENTIFIER = "pui";
					////player update
	public static String ADD_MULTIPLAYER = "^amp^";
	public static String REMOVE_MULTIPLAYER = "^rmp^";
	public static String PLAYER_POSITION_UPDATE_IDENTIFIER = "^ppu^";
	public static String SET_USERNAME = "^su^";
	public static String PLAYER_KILLED = "^pk^";
	//////////////////////////////////////////////////////
	
						///MAP ITEM
	//////////////////////////////////////////////////////
	public static String MAP_ITEM_PACKET_IDENTIFIER = "mii";
					///map item
	public static String SPAWN_MAP_OBJECT = "^smo^";
	public static String REMOVE_MAP_OBJECT = "^rmo^";
	public static String CREATE_BULLET = "^cb^";
	public static String END_BULLET_TYPE_IDENTIFIER = "^%^";
	public static String END_ORIGIN_LOCATION_VALUE = "^@^";
	//////////////////////////////////////////////////////
	
	
						///SERVER CLIENT INFO
	//////////////////////////////////////////////////////	
	public static String SERVER_CLIENT_INFO_PACKET_IDENTIFIER = "sci";	
					 ////server client info
	public static String DISPLAY_GLOBAL_MESSAGE = "^dgm^";
	public static String GUI_STRING_UPDATE = "^gsu^";
	public static String QUE_GAME_SEARCH = "^qgs^";
	public static String GAME_FOUND = "^gf^";
	//////////////////////////////////////////////////////
	
}
