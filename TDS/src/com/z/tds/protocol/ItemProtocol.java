package com.z.tds.protocol;

public class ItemProtocol {	
	
	/////BASIC ITEMS/////////////////////////////
	public static String BANDAID_ITEM_DESCRIPTION = "Bandage";
	public static String SNIPER_308_BULLET_ITEM_DESCRIPTION = ".308 Bullet";
	public static String AK_47_BULLET_ITEM_DESCRIPTION = "AK 47 Bullet";
	public static String PISTOL_45_BULLET_ITEM_DESCRIPTION = ".45 Pistol Bullet";
	////////////////////////////////////////////
	
	
	////WEAPONS/////////////////////////////////
	public static String SNIPER_308_WEAPON_DESCRIPTION = ".308 Hunting Rifle";
	public static String PISTOL_45_WEAPON_DESCRIPTION = ".45 Pistol";
	public static String AK47_WEAPON_DESCRIPTION = "AK 47";
	public static String PLAYER_FIST_DESCRIPTION = "Fists";
	////////////////////////////////////////////
}
