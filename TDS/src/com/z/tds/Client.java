package com.z.tds;

import java.util.ArrayList;

import javax.swing.SwingUtilities;

import com.z.tds.gui.Display;
import com.z.tds.gui.Screen;
import com.z.tds.protocol.NetworkProtocol;
import com.z.tds.protocol.TimeProtocol;
import com.z.tds.utilities.ImageResources;
import com.z.tds.worlds.tiles.MapTile;

public class Client extends Thread {
	
	private boolean isRunning = false;
	private ArrayList<Screen> screens;
	
	private long lastScreenUpdate = 0;
	private long lastTick = 0;
	private long lastKeyUpdate = 0;
	
	private Display display;
	private ImageResources imageResources;
	
	private ArrayList<MapTile> gameMapTiles;
	
	private int screenWidth = 720, screenHeight = 480;
	
	
	public Client() {
		imageResources = new ImageResources();
		display = new Display(this);		
		run();
	}
	
	@Override
	public void run() {
		isRunning = true;
		//sendLoginAttemptToServer("username", "password");
		//sendCreateBulletToServer("username", "AK47", 1, 2, 4, 5);
		while(isRunning) {
			long now = getCurrentMilliSecond();
		}
			
	}

	
	
	public Display getDisplay() {
		return display;
	}
	
	public ImageResources getImageResources() {
		return imageResources;
	}
	
	public long getCurrentMilliSecond() {
		return System.currentTimeMillis();
	}

	public float getScreenWidth() {
		return screenWidth;
	}
	
	public float getScreenHeight() {
		return screenHeight;
	}
	
	public ArrayList<MapTile> getGameMapTilesList() {
		return gameMapTiles;
	}
	
	public void setGameMapTilesList(ArrayList<MapTile> tiles) {
		this.gameMapTiles = tiles;
	}


}
