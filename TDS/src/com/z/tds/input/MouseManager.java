package com.z.tds.input;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import com.z.tds.Client;
import com.z.tds.utilities.pathfinding.PathFinder;
import com.z.tds.worlds.tiles.MapTile;

public class MouseManager implements MouseListener, MouseMotionListener {

	private float mouseX, mouseY;
	private Client client;
	private ArrayList<MapTile> tilesOnScreen;

	public MouseManager(Client client) {
		this.client = client;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		setMouseX(e.getX());
		setMouseY(e.getY());
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		setMouseX(e.getX());
		setMouseY(e.getY());
		//tilesOnScreen = client.getDisplay().getCurrentScreen().getMapTilesArrayList();
		if (client.getDisplay().getCurrentScreen().getScreenDescription().contains("test basic game screen")) {
			Rectangle mouseBounds = new Rectangle((int) getMouseX() - 4, (int) getMouseY() - 8, 1, 1);
			for (MapTile tile : tilesOnScreen) {
				if (isTileOnScreen(tile)) {
					if (mouseBounds.intersects(tile.getTileBounds())) {
						client.getDisplay().getCurrentScreen().setMouseCurrentTileLocation(tile);
						tile.setColor(Color.PINK);
					}
				}
			}
		}
	}

	public boolean isTileOnScreen(MapTile tile) {
		Rectangle screenBounds = new Rectangle(client.getDisplay()
				.getCurrentScreen().getScreenCamera().getXOffset(),
				client.getDisplay().getCurrentScreen().getScreenCamera()
						.getYOffset(), client.getDisplay().getCurrentScreen()
						.getWidth(), client.getDisplay().getCurrentScreen()
						.getHeight());
		if (screenBounds.contains(tile.getRenderX() + 8, tile.getRenderY() + 8)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		setMouseX(e.getX());
		setMouseY(e.getY());
		System.out.println("attempting search");
		ArrayList<MapTile> tiles = client.getGameMapTilesList();

		int playerXLocation = getClientPlayerLocation()[0];
		int playerYLocation = getClientPlayerLocation()[1];

		int mouseScreenLocationX = getMouseScreenLocation()[0];
		int mouseScreenLocationY = getMouseScreenLocation()[1];

		MapTile playerCurrentMapTileLocation = client.getDisplay()
				.getCurrentScreen()
				.getMapTileAt(playerXLocation, playerYLocation);
		long now = client.getCurrentMilliSecond();
		PathFinder.findPath(playerCurrentMapTileLocation, client.getDisplay()
				.getCurrentScreen().getMouseCurrentMapTileLocation());
		long afterPath = client.getCurrentMilliSecond();
		// System.out.println("path found in total of '" + (afterPath - now) +
		// "' ms.");
		// client.getDisplay().getCurrentScreen().getMouseCurrentMapTileLocation().setColor(Color.PINK);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		setMouseX(e.getX());
		setMouseY(e.getY());
	}

	@Override
	public void mouseExited(MouseEvent e) {
		setMouseX(e.getX());
		setMouseY(e.getY());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		setMouseX(e.getX());
		setMouseY(e.getY());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		setMouseX(e.getX());
		setMouseY(e.getY());
	}

	public float getMouseX() {
		return mouseX;
	}

	public void setMouseX(float mouseX) {
		this.mouseX = mouseX;
	}

	public float getMouseY() {
		return mouseY;
	}

	public void setMouseY(float mouseY) {
		this.mouseY = mouseY;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getXOffset() {
		return client.getDisplay().getCurrentScreen().getScreenCamera()
				.getXOffset();
	}

	public int getYOffset() {
		return client.getDisplay().getCurrentScreen().getScreenCamera()
				.getYOffset();
	}

	public int[] getClientPlayerLocation() {
		int[] location = new int[2];
		int xOffset = client.getDisplay().getCurrentScreen().getScreenCamera()
				.getXOffset();
		int yOffset = client.getDisplay().getCurrentScreen().getScreenCamera()
				.getYOffset();
		int playerX = client.getDisplay().getCurrentScreen()
				.getScreenClientPlayer().getPlayerXLocationForRotation();
		int playerY = client.getDisplay().getCurrentScreen()
				.getScreenClientPlayer().getPlayerYLocationForRotation();
		location[0] = (playerX + xOffset) / 16;
		location[1] = (playerY + yOffset) / 16;
		return location;
	}

	public int[] getMouseScreenLocation() {
		int[] location = new int[2];
		location[0] = (int) ((getMouseX() + getXOffset()) / 16);
		location[1] = (int) ((getMouseY() + getYOffset()) / 16);
		return location;
	}

	public int[] getMouseBoundsLocation() {
		int[] location = new int[2];
		location[0] = (int) getMouseX() + getXOffset();
		location[1] = (int) getMouseY() + getYOffset();
		return location;
	}

	public void setTilesOnScreen(ArrayList<MapTile> tiles) {
		this.tilesOnScreen = tiles;
	}

}
