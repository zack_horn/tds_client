package com.z.tds.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.z.tds.Client;
import com.z.tds.gui.Screen;

public class KeyManager implements KeyListener {

	private Screen screen;
	private boolean[] keys;
	private boolean up, down, left, right;
	private boolean upArrow, downArrow, leftArrow, rightArrow, shift;
	private Client client;
	private int runMultiplier = 1;

	public KeyManager(Client client) {
		this.client = client;
		keys = new boolean[256];
	}

	public void update() {
		if (isInGameScreen()) {
			int xDirection = 0;
			int yDirection = 0;
			up = keys[KeyEvent.VK_W];
			upArrow = keys[KeyEvent.VK_UP];
			down = keys[KeyEvent.VK_S];
			downArrow = keys[KeyEvent.VK_DOWN];
			left = keys[KeyEvent.VK_A];
			leftArrow = keys[KeyEvent.VK_LEFT];
			right = keys[KeyEvent.VK_D];
			rightArrow = keys[KeyEvent.VK_RIGHT];
			shift = keys[KeyEvent.VK_SHIFT];
			if (shift) {
				runMultiplier = 2;
			}
			if (up || upArrow) {
				if (up && left) {
					yDirection--;
					yDirection--;
					xDirection--;
					xDirection--;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (up && right) {
					yDirection--;
					xDirection++;
					yDirection--;
					xDirection++;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (up && !right && !left) {
					yDirection--;
					yDirection--;
					
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				return;
			}
			if (down || downArrow) {
				if (down && right) {
					xDirection++;
					yDirection++;
					xDirection++;
					yDirection++;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (down && left) {
					yDirection++;
					xDirection--;
					yDirection++;
					xDirection--;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (down && !left && !right) {
					yDirection++;
					yDirection++;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				return;

			}
			if (left || leftArrow) {
				if (left && up) {
					xDirection--;
					yDirection--;
					xDirection--;
					yDirection--;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (left && down) {
					xDirection--;
					yDirection++;
					xDirection--;
					yDirection++;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (left && !down && !up) {
					xDirection--;
					xDirection--;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				return;
			}
			if (right || rightArrow) {
				if (right && up) {
					xDirection++;
					yDirection--;
					xDirection++;
					yDirection--;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (right && down) {
					xDirection++;
					yDirection++;
					xDirection++;
					yDirection++;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				if (right && !up && !down) {
					xDirection++;
					xDirection++;
					getCurrentScreen().getScreenClientPlayer().updatePlayerXY(
							xDirection * runMultiplier,
							yDirection * runMultiplier);
					runMultiplier = 1;
				}
				return;
			}

		}

	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public Screen getCurrentScreen() {
		return screen;
	}

	public boolean isInGameScreen() {
		if (screen.getScreenDescription().contains("test basic game screen")) {
			return true;
		}
		return false;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		keys[e.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// System.out.println("pressed3: " + arg0.getKeyChar());
	}

}
