package com.z.tds.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.z.tds.Client;
import com.z.tds.gui.screens.ConnectionPanelScreen;
import com.z.tds.gui.screens.PreGameLobbyScreen;
import com.z.tds.gui.screens.TestBasicGameScreen;
import com.z.tds.input.KeyManager;
import com.z.tds.input.MouseManager;

public class Display {
	
	private Client client;
	private JFrame frame;
	private Screen screen;
	private KeyManager keyManager;
	private MouseManager mouseManager;
	private ConnectionPanelScreen connectionPanelScreen;
	private PreGameLobbyScreen preGameLobbyScreen;
	private TestBasicGameScreen testBasicGameScreen;
	
		
	
	public Display(Client client) {
		this.client = client;
		init();
	}
	
	public void init() {
		frame = configureDisplayFrame();
	}
	
	public JFrame configureDisplayFrame() {
		mouseManager = new MouseManager(client);
		keyManager = new KeyManager(client);
		JFrame jFrame = new JFrame("TDS BR v0.1");
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setResizable(false);
		jFrame.setLayout(new BorderLayout());
		jFrame.setVisible(true);		
		jFrame.addKeyListener(keyManager);
		jFrame.addMouseListener(mouseManager);
		jFrame.addMouseMotionListener(mouseManager);
		jFrame.setFocusTraversalKeysEnabled(false);
		jFrame.validate();
		return jFrame;
	}
	
	public KeyManager getKeyManager() {
		return keyManager;
	}
	
	public MouseManager getMouseManager() {
		return mouseManager;		
	}
	
	public Screen getCurrentScreen() {
		return screen;
	}
	
	public Screen getBasicTestMapScreen() {
		return testBasicGameScreen;
	}	
	
	public void setCurrentScreen(Screen screen) {
		this.screen = screen;
		keyManager.setScreen(screen);
		frame.requestFocus();
		frame.pack();
		frame.validate();	
	}

}
