package com.z.tds.gui;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.z.tds.Client;
import com.z.tds.objects.PlayerClient;
import com.z.tds.utilities.Camera;
import com.z.tds.worlds.World;
import com.z.tds.worlds.tiles.MapTile;

public abstract class Screen extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1157180708838420596L;
	
	
	protected Client client;
	protected World world;
	protected Camera camera;
	protected int FPS = 0;
	protected int width = 720, height = 480;
	protected MapTile mouseCurrentTileLocation;
	
	public Screen(Client client) {
		this.client = client;
	}
	
	public abstract void render(Graphics2D graphics2D, int xOffset, int yOffset);
	
	public abstract void update();
	
	public abstract void tick();
	
	public Client getClient() {
		return client;
	}
	
	public String getScreenDescription() {
		return "";
	}
	
	public int getScreenWidth() {
		return width;
	}
	
	public int getScreenHeight() {
		return height;
	}
	
	public Camera getScreenCamera() {
		return null;
	}
	
	public PlayerClient getScreenClientPlayer() {
		return null;
	}
	
	public abstract ArrayList<MapTile> getMapTilesArrayList();
	
	public abstract MapTile getMapTileAt(int x, int y);
	
	public void setMouseCurrentTileLocation(MapTile mapTile) {
		this.mouseCurrentTileLocation = mapTile;
	}
	
	public MapTile getMouseCurrentMapTileLocation() {
		return mouseCurrentTileLocation;
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width, height);		
	}
	
	
	
	
}
