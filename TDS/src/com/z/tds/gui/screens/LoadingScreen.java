package com.z.tds.gui.screens;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import com.z.tds.Client;
import com.z.tds.gui.Screen;
import com.z.tds.worlds.tiles.MapTile;

public class LoadingScreen extends Screen {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Color backgroundColor, loadingBoxColor, loadingBarColor, textColor;
	private int percentOfLoadingComplete = 0;
	private Rectangle loadingBoxRectangle, loadingBarRectangle;

	public LoadingScreen(Client client) {
		super(client);
	}

	@Override
	public void render(Graphics2D graphics2d, int xOffset, int yOffset) {
		Color previousColor = graphics2d.getColor();
		graphics2d.setColor(backgroundColor);
		graphics2d.fillRect(0, 0, getWidth(), getHeight());		
		graphics2d.setColor(loadingBoxColor);
		graphics2d.drawRect(loadingBoxRectangle.x, loadingBoxRectangle.y, loadingBarRectangle.width, loadingBoxRectangle.height);
		graphics2d.setColor(textColor);
		graphics2d.drawString("LOADING.....", 50, 50);
		graphics2d.setColor(previousColor);
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<MapTile> getMapTilesArrayList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MapTile getMapTileAt(int x, int y) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void initLoadingBox() {
		loadingBoxRectangle = new Rectangle(50, 50, 120, 120);
		loadingBarRectangle = new Rectangle(60, 60, 60, 60);
	}
	
	public void initColors() {
		backgroundColor = Color.BLACK;
		loadingBoxColor = Color.GRAY;
		textColor = Color.WHITE;
	}

}
