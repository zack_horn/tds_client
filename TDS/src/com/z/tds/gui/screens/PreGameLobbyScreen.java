package com.z.tds.gui.screens;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.z.tds.Client;
import com.z.tds.gui.Screen;
import com.z.tds.gui.menu.MenuPreGameLobbyPlayerInfo;
import com.z.tds.worlds.tiles.MapTile;

public class PreGameLobbyScreen extends Screen {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BufferedImage screenBackground;
	private ArrayList<MenuPreGameLobbyPlayerInfo> playerInfos;

	public PreGameLobbyScreen(Client client) {
		super(client);
		init();
	}
	
	@Override
	public void paintComponent(Graphics graphics) {
		Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.clearRect(0, 0, width, height);
		render(graphics2D, 0, 0);
		repaint();
	}
	
	@Override
	public void render(Graphics2D graphics2d, int xOffset, int yOffset) {
		graphics2d.drawImage(screenBackground, 0, 0, null);
		for(MenuPreGameLobbyPlayerInfo playerInfo: playerInfos) {
			playerInfo.render(graphics2d, 0, 0);
		}
		
	}

	@Override
	public void update() {
		for(MenuPreGameLobbyPlayerInfo playerInfo: playerInfos) {
			playerInfo.update();
		}
	}
	

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String getScreenDescription() {
		 return "pregame lobby screen";
	}
	
	public void init() {
		MenuPreGameLobbyPlayerInfo playerInfoOne = new MenuPreGameLobbyPlayerInfo(client, "USER_1", 1);
		MenuPreGameLobbyPlayerInfo playerInfoTwo = new MenuPreGameLobbyPlayerInfo(client, "USER_2", 2);
		MenuPreGameLobbyPlayerInfo playerInfoThree = new MenuPreGameLobbyPlayerInfo(client, "USER_3", 3);
		MenuPreGameLobbyPlayerInfo playerInfoFour = new MenuPreGameLobbyPlayerInfo(client, "USER_4", 4);
		playerInfos = new ArrayList<MenuPreGameLobbyPlayerInfo>();
		playerInfos.add(playerInfoOne);
		playerInfos.add(playerInfoTwo);
		playerInfos.add(playerInfoThree);
		playerInfos.add(playerInfoFour);
		screenBackground = client.getImageResources().getPreGameLobbyBackgroundGUI();		
	}

	@Override
	public ArrayList<MapTile> getMapTilesArrayList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MapTile getMapTileAt(int x, int y) {
		// TODO Auto-generated method stub
		return null;
	}

}
