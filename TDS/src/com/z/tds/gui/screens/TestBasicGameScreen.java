package com.z.tds.gui.screens;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import com.z.tds.Client;
import com.z.tds.gui.Screen;
import com.z.tds.input.KeyManager;
import com.z.tds.objects.Entity;
import com.z.tds.objects.PlayerClient;
import com.z.tds.protocol.TimeProtocol;
import com.z.tds.utilities.Camera;
import com.z.tds.worlds.MapLoader;
import com.z.tds.worlds.tiles.MapTile;
import com.z.tds.worlds.tiles.Tile;

public class TestBasicGameScreen extends Screen {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<MapTile> gameMap;
	private ArrayList<Entity> entitiesOnMap;
	private MapLoader mapLoader;
	private Camera camera;
	private PlayerClient playerClient;
	private KeyManager keyManager;
	
	private long lastFPSCheck;
	private int fpsCount;
	private int currentFPS;
	
	private int tilesRendered;
	private int entitiesRendered;

	public TestBasicGameScreen(Client client) {
		super(client);
		gameMap = new ArrayList<MapTile>();
		entitiesOnMap = new ArrayList<Entity>();
		mapLoader = new MapLoader(client);
		camera = new Camera(this);
		playerClient = new PlayerClient(0, 0, "USER_1", client.getImageResources().getBasicPlayerSprite(), client, this);
		entitiesOnMap.add(playerClient);
		initMap();
	}
	
	@Override
	public void paintComponent(Graphics graphics) {
		Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.clearRect(0, 0, width, height);
		render(graphics2D, 0, 0);
		Color previousColor = graphics2D.getColor();
		graphics2D.setColor(Color.GREEN);
		graphics2D.drawString("Fps: " + currentFPS, 25, 25);
		graphics2D.drawString("Tiles Rendered: " + tilesRendered, 25, 45);
		graphics2D.drawString("Entities Rendered: " + entitiesRendered, 25, 65);
		graphics2D.setColor(previousColor);
		graphics2D.dispose();
		repaint();
	}

	@Override
	public void render(Graphics2D graphics2d, int xOffset, int yOffset) {
		long now = client.getCurrentMilliSecond();
		if(now - lastFPSCheck >= TimeProtocol.SECOND) {
			//System.out.println(fpsCount);
			currentFPS = fpsCount;
			fpsCount = 0;			
			lastFPSCheck = now;
		}
		renderTiles(graphics2d, camera.getXOffset(), camera.getYOffset());
		renderEntities(graphics2d, camera.getXOffset(), camera.getYOffset());
		fpsCount++;
	}

	@Override
	public void update() {
		for(MapTile mapTile: gameMap) {
			mapTile.update();
		}
	}
	

	@Override
	public void tick() {
		tickTiles();
	}
	
	@Override
	public PlayerClient getScreenClientPlayer() {
		return playerClient;
	}
	
	public void renderTiles(Graphics2D graphics2D, int xOffset, int yOffset) {		
		int renderingTileCount = 0;
		for(Tile tile: gameMap) {	
			if(isTileOnScreen((MapTile) tile)) {
				tile.render(graphics2D, camera.getXOffset(), camera.getYOffset());
				renderingTileCount++;
			}				
		}
		tilesRendered = renderingTileCount;
	}
	
	public void renderEntities(Graphics2D graphics2D, int xOffset, int yOffset) {
		int renderingEntitiesCount = 0;		
		for(Entity entity: entitiesOnMap) {
			entity.render(graphics2D, xOffset, yOffset);
			renderingEntitiesCount++;
		}
		entitiesRendered = renderingEntitiesCount;
	}
	
	public void tickTiles() {
		for(Tile tile: gameMap) {	
			if(isTileOnScreen((MapTile) tile)) {
				tile.tick();
			}				
		}
	}
	
	public boolean isEntityOnScreen(Entity entity) {
		Rectangle screenBounds = new Rectangle(camera.getXOffset(), camera.getYOffset(), getWidth(), getHeight());
		if(screenBounds.contains(entity.getRenderX() + 8, entity.getRenderY() + 8)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isTileOnScreen(MapTile tile) {
		Rectangle screenBounds = new Rectangle(camera.getXOffset(), camera.getYOffset(), getWidth(), getHeight());
		if(screenBounds.contains(tile.getRenderX() + 8, tile.getRenderY() + 8)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public Camera getScreenCamera() {
		return camera;
	}
	
	public String getScreenDescription() {
		return "test basic game screen";
	}
	
	
	
	@Override
	public ArrayList<MapTile> getMapTilesArrayList() {
		return gameMap;
	}
	
	public void initMap() {
		gameMap = mapLoader.loadMap("/maps/basicTestMap.mf");
		client.setGameMapTilesList(gameMap);
	}

	@Override
	public MapTile getMapTileAt(int x, int y) {
		int indexOfRequestedTile = gameMap.indexOf(new MapTile(x, y));
		return gameMap.get(indexOfRequestedTile);
	}

}
