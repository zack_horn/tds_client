package com.z.tds.gui.screens;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import com.z.tds.Client;
import com.z.tds.gui.Screen;
import com.z.tds.worlds.tiles.MapTile;

public class ConnectionPanelScreen extends Screen {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextArea usernameTextArea;
	private JPasswordField passwordField;
	private JButton loginButton, createAccountButton;
	
	public ConnectionPanelScreen(Client client) {
		super(client);
		configureScreenLayout();
		
	}
	
	

	@Override
	public void render(Graphics2D graphics2d, int xOffset, int yOffset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(140, 200);		
	}
	
	@Override
	public String getScreenDescription() {
		 return "login panel screen";
	}
	
	public void configureScreenLayout() {
		JPanel usernamePanel = new JPanel();
		usernameTextArea = new JTextArea(1,10);
		passwordField = new JPasswordField(10);
		createAccountButton = new JButton("Create Account");
		loginButton = new JButton("Login");
		
		////temp loginbutton listener
		loginButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//client.getDisplay().setScreenToPreGame();
			//	client.getDisplay().setScreenToBasicTestMapScreen();
			}
		});
		
		///
		
		usernamePanel.setBorder(new TitledBorder("Username:"));
		usernamePanel.add(usernameTextArea);
		JPanel passwordPanel = new JPanel();
		passwordPanel.setBorder(new TitledBorder("Password:"));
		passwordPanel.add(passwordField);
		addObjectsToPanel(usernamePanel, passwordPanel);
	}
	
	public void addObjectsToPanel(JPanel usernamePanel, JPanel passwordPanel) {
		this.setLayout(new GridLayout(4, 0));
		this.add(usernamePanel);
		this.add(passwordPanel);
		this.add(loginButton);
		this.add(createAccountButton);
	}



	@Override
	public ArrayList<MapTile> getMapTilesArrayList() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public MapTile getMapTileAt(int x, int y) {
		// TODO Auto-generated method stub
		return null;
	}

}
