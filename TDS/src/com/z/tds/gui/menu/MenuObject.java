package com.z.tds.gui.menu;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import com.z.tds.Client;
import com.z.tds.objects.items.Item;

public abstract class MenuObject {
	
	protected float x, y;
	protected Menu menu;
	protected String action;
	protected Color backgroundColor, textColor;
	protected Item item;
	protected int index;
	protected Client client;
	
	public MenuObject(Client client) {
		this.client = client;
	}
	
	public abstract void render(Graphics2D graphics2D, float x, float y);
	
	public abstract void update();
	
	public abstract Rectangle getMenuObjectBounds();

	private float getX() {
		return x;
	}

	private void setX(float x) {
		this.x = x;
	}

	private float getY() {
		return y;
	}

	private void setY(float y) {
		this.y = y;
	}

	private Menu getMenu() {
		return menu;
	}

	private void setMenu(Menu menu) {
		this.menu = menu;
	}

	private String getAction() {
		return action;
	}

	private void setAction(String action) {
		this.action = action;
	}

	private Color getBackgroundColor() {
		return backgroundColor;
	}

	private void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	private Color getTextColor() {
		return textColor;
	}

	private void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	private Item getItem() {
		return item;
	}

	private void setItem(Item item) {
		this.item = item;
	}

	private int getIndex() {
		return index;
	}

	private void setIndex(int index) {
		this.index = index;
	}
	
	private Client getClient() {
		return client;
	}
	
	
	/* mock up of how to get possible actions
	 * 
	 * 
	 * public String[] getPossibleActions() {
		String[] actions = new String[2];
		if(item != null) {
			if(!item.isAWeapon()) {
				actions[0] = MenuProtocol.USE_ACTION;
				actions[1] = MenuProtocol.DROP_ACTION;
			}
			if(item.isAWeapon()) {
				actions[0] = MenuProtocol.EQUIP_ACTION;
				actions[1] = MenuProtocol.DROP_ACTION;
			}
			return actions;
		}
		actions[0] = "NOTHING";
		actions[1] = "NOTHING";
		return actions;
	}
	 */
}
