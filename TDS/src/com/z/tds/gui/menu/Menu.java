package com.z.tds.gui.menu;

import java.awt.Color;
import java.awt.Graphics2D;

import com.z.tds.Client;
import com.z.tds.objects.items.Item;

public abstract class Menu {
	
	protected Client client;
	protected float x, y;
	protected Color backgroudColor, textColor;
	protected String selectedAction;
	protected Item selectedItem;
	protected MenuObject selectedMenuObject;
	
	
	public abstract void render(Graphics2D graphics2D, float x, float y);
	
	public abstract void update();	
	

	private Client getClient() {
		return client;
	}

	private void setClient(Client client) {
		this.client = client;
	}

	private float getX() {
		return x;
	}

	private void setX(float x) {
		this.x = x;
	}

	private float getY() {
		return y;
	}

	private void setY(float y) {
		this.y = y;
	}

	private Color getBackgroudColor() {
		return backgroudColor;
	}

	private void setBackgroudColor(Color backgroudColor) {
		this.backgroudColor = backgroudColor;
	}

	private Color getTextColor() {
		return textColor;
	}

	private void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	private String getSelectedAction() {
		return selectedAction;
	}

	private void setSelectedAction(String selectedAction) {
		this.selectedAction = selectedAction;
	}

	private Item getSelectedItem() {
		return selectedItem;
	}

	private void setSelectedItem(Item selectedItem) {
		this.selectedItem = selectedItem;
	}

	private MenuObject getSelectedMenuObject() {
		return selectedMenuObject;
	}

	private void setSelectedMenuObject(MenuObject selectedMenuObject) {
		this.selectedMenuObject = selectedMenuObject;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
