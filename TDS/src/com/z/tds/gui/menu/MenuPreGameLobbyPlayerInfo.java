package com.z.tds.gui.menu;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.z.tds.Client;
import com.z.tds.protocol.GuiProtocol;
import com.z.tds.utilities.ImageResources;

public class MenuPreGameLobbyPlayerInfo extends MenuObject {
	
	private int guiIndex;
	private String username;
	private Color previousColor;
	private BufferedImage countryFlag;
	
	public MenuPreGameLobbyPlayerInfo(Client client, String username, int guiIndex) {
		super(client);
		this.guiIndex = guiIndex;
		this.username = username;
		init();
	}

	private BufferedImage currentBackgroundSprite, basicButtonOpacityFull, basicButtonOpacityHalf, basicButtonPlaceHolder;
	private int width = 192, height = 32;

	@Override
	public void render(Graphics2D graphics2d, float x, float y) {
		
		//Draw place holding button(bufferedImage)
		graphics2d.drawImage(basicButtonPlaceHolder, GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_X_VALUE, GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_Y_VALUE * guiIndex, null);
		
		//Get color then change to display username string
		previousColor = graphics2d.getColor();
		graphics2d.setColor(Color.RED);
		graphics2d.drawString(username, GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_X_VALUE + 48, (GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_Y_VALUE * guiIndex) + 20);
		graphics2d.setColor(previousColor);
		
		//Draw country of client next to username string
		graphics2d.drawImage(countryFlag, GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_X_VALUE + 24, (GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_Y_VALUE * guiIndex) + 10, null);		
	}

	@Override
	public void update() {
		Rectangle mouseBounds = new Rectangle((int)client.getDisplay().getMouseManager().getMouseX(), (int)client.getDisplay().getMouseManager().getMouseY(), 2, 2);
		if(mouseBounds.intersects(getMenuObjectBounds())) {
			basicButtonPlaceHolder = basicButtonOpacityFull;
		} else {
			basicButtonPlaceHolder = basicButtonOpacityHalf;
		}		
	}

	@Override
	public Rectangle getMenuObjectBounds() {
		return new Rectangle(GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_X_VALUE, (GuiProtocol.PRE_GAME_LOBBY_PLAYER_INFO_MENUOBJECT_Y_VALUE  * guiIndex) + 24, basicButtonOpacityFull.getWidth(), basicButtonOpacityFull.getHeight());
	}
	
	
	
	public void init() {
		ImageResources imageResources = new ImageResources();
		basicButtonOpacityFull = imageResources.getBasicButtonSpriteOpacityFull();
		basicButtonOpacityHalf = imageResources.getBasicButtonSpriteOpacityHalf();
		currentBackgroundSprite = imageResources.getPreGameLobbyBackgroundGUI();
		basicButtonPlaceHolder = basicButtonOpacityHalf;
		countryFlag = imageResources.getCountryFlag("us");
	}

}
