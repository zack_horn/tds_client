package com.z.tds.objects;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.z.tds.Client;

public abstract class Entity {
	
	protected float x, y;
	protected int width = 16, height = 16;
	protected Rectangle entityBounds;
	protected int health;
	protected BufferedImage entitySprite;
	protected Client client;
	protected String username;
	
	public Entity(float x, float y, String username, BufferedImage entitySprite, Client client) {
		this.x = x;
		this.y = y;
		this.entitySprite = entitySprite;
		this.client = client;
		this.health = 100;
		this.username = username;
	}
	
	public abstract void render(Graphics2D graphics2D, int xOffset, int yOffset);
	
	public abstract void tick();
	
	public abstract void update();
	
	public void moveXPosition(float amountToMove) {
		x += amountToMove;
	}
	
	public void moveYPosition(float amountToMove) {
		y += amountToMove;
	}
	
	public void doDamageToEntity(int damageToAssign) {
		this.health -= damageToAssign;
	}
	
	public String getUsername() {
		return username;
	}
	
	public float getTileLocationX() {
		return x / 16;
	}
	
	public float getTileLocationY() {
		return y / 16;
	}
	
	public float getRenderX() {
		return x * 16;
	}
	
	public float getRenderY() {
		return y * 16;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public int getWidthOfEntity() {
		return width;
	}
	
	public int getHeightOfEntity() {
		return height;
	}
	
	public Client getClient() {
		return client;
	}

	private int getHealth() {
		return health;
	}

	private void setHealth(int health) {
		this.health = health;
	}

	private BufferedImage getEntitySprite() {
		return entitySprite;
	}

	private void setEntitySprite(BufferedImage entitySprite) {
		this.entitySprite = entitySprite;
	}

	private void setX(float x) {
		this.x = x;
	}

	private void setY(float y) {
		this.y = y;
	}

}
