package com.z.tds;

import java.util.ArrayList;

import com.z.tds.objects.Entity;
import com.z.tds.worlds.MapLoader;
import com.z.tds.worlds.tiles.MapTile;

public abstract class Game extends Thread {
	
	private Client client;
	
	private ArrayList<Entity> entitiesInGame;
	private ArrayList<MapTile> tilesInGame;
	
	private String mapFilePath;
	private Entity clientPlayer;
	
	
	public Game(Client client, String mapFilePath) {
		this.client = client;
		this.mapFilePath = mapFilePath;
	}
	
	@Override
	public void run() {
		
	}
	
	public void requestEntitiesInGameFromServer() {
		
	}
	
	
	public void initGameMap() {
		MapLoader mapLoader = new MapLoader(client);
		tilesInGame = mapLoader.loadMap(mapFilePath);
	}
	
	
	
	

}
