package com.z.tds.worlds.tiles;

import java.awt.image.BufferedImage;

import com.z.tds.Client;

public class GrassTile extends StaticTile {

	public GrassTile(int x, int y, boolean walkable, boolean isSolid, Client client, BufferedImage sprite) {
		super(x, y, walkable, isSolid, client, sprite);
	}

}
