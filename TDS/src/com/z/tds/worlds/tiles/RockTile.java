package com.z.tds.worlds.tiles;

import java.awt.image.BufferedImage;

import com.z.tds.Client;

public class RockTile extends StaticTile {

	public RockTile(int x, int y, boolean walkable, boolean isSolid, Client client, BufferedImage sprite) {
		super(x, y, walkable, isSolid, client, sprite);
	}

}
