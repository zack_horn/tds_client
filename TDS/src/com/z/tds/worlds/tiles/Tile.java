package com.z.tds.worlds.tiles;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import com.z.tds.Client;

public abstract class Tile {
	 
    public int x, y;
    protected boolean isSolid;
    protected boolean isWalkable;
    protected Client client;
 
    public Tile(int x, int y, boolean isWalkable, boolean isSolid, Client client) {
    	this.isSolid = isSolid;
    	this.isWalkable = isWalkable;
    	this.client = client;
        this.x = x;
        this.y = y;
    }
 
    public abstract void render(Graphics2D graphics, int xOffset, int yOffset);
 
    public abstract void tick();
    
    public abstract void update();
    
    /**
     * Returns absolute x location
     * @return int
     */
    public int getX() {
        return x;
    }
    
    /**
     * Set tile absolute x location
     * @param x
     */
    public void setX(int x) {
        this.x = x;
    }
    
    /**
     * Returns absolute y location
     * @return
     */
    public int getY() {
        return y;
    }
    
    /**
     * Set tile absolute y location
     * @param y
     */
    public void setY(int y) {
        this.y = y;
    }
    
    /**
     * Returns x location for graphic package to render at
     * @return int
     */
    public int getRenderX() {
    	return x * 16;    	
    }
    
    /**
     * Returns y location for graphics package to render at
     * @return int
     */
    public int getRenderY() {
    	return y * 16;
    }
    
    /**
     * Returns whether or not tile is solid
     * @return
     */
    public boolean isSolid() {
    	return isSolid;
    }
    
    public boolean isWalkable() {
    	return isWalkable;
    }
    
    public Rectangle getTileBounds() {
    	//return new Rectangle((x * 16) - client.getXOffset(), (y * 16) - client.getYOffset(), 16, 16);
    	return new Rectangle();
    }
 
    /**
     * Returns boolean determined by x and y location of tiles compared
     */
    @Override
    public boolean equals(Object object) {
        MapTile tile = (MapTile) object;
        return tile.getX() == this.x && tile.getY() == this.y;
    }
    
    /**
     * Currently used only in pathfinding
     */
    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
