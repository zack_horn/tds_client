package com.z.tds.worlds.tiles;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.z.tds.Client;

public class WaterTile extends AnimatedTile {

	public WaterTile(int x, int y, boolean isWalkable, boolean isSolid,	Client client, ArrayList<BufferedImage> sprites) {
		super(x, y, isWalkable, isSolid, client, sprites);
	}

}
