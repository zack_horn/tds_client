package com.z.tds.worlds.tiles;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import com.z.tds.Client;
import com.z.tds.objects.items.Item;

public class MapTile extends Tile {
	
	private Rectangle tileCollisionBounds;
	private ArrayList<Item> itemsOnTile;
	 
    public enum State {
        DEFAULT, SEARCHED, PATHED
    }
 
    private State state;
    private ArrayList<MapTile> neighbors;
 
    private BufferedImage sprite;
    private Color color;
 
    public MapTile(int x, int y, boolean walkable, boolean isSolid, Client client) {
        super(x, y, walkable, isSolid, client);       
        this.state = State.DEFAULT; 
        itemsOnTile = new ArrayList<Item>();
        
    }
    
	@Override
	public void update() {
		setColor(null);
		
	}
		
	public int getMouseBoundsX() {
		return (int)client.getDisplay().getMouseManager().getMouseX();
	}
		
	public int getMouseBoundsY() {
		return (int)client.getDisplay().getMouseManager().getMouseY();		
	}
	
	
	public Rectangle getTileBounds() {
		return new Rectangle(getRenderX() - client.getDisplay().getCurrentScreen().getScreenCamera().getXOffset(), getRenderY() - client.getDisplay().getCurrentScreen().getScreenCamera().getYOffset() + 16, 16, 16);
	}
	
	public Rectangle getTileCollisionBounds() {
		return new Rectangle(x * 16 - client.getDisplay().getCurrentScreen().getScreenCamera().getXOffset(), y * 16 - client.getDisplay().getCurrentScreen().getScreenCamera().getYOffset(), 16, 16);
		//	return  new Rectangle((int) (x * 16 - client.getXOffset() + xMovement), (int) (y * 16 - client.getYOffset() + yMovement), 16, 16);
	}
	
	public void addItemToTile(Item item) {
	/*	item.setX(getRenderX());
		item.setY(getRenderY());
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				itemsOnTile.add(item);
			}
		}); */
	}
	
	public void removeItemFromTile(Item item) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				itemsOnTile.remove(item);
			}
		});
	}
	
	public ArrayList<Item> getItemsOnTile() {
		return itemsOnTile;
	}
 
    public Color getColor() {
        return color;
    } 
 
    public void setColor(Color color) {
        this.color = color;
    }
 
    public BufferedImage getSprite() {
        return sprite;
    }
 
    public void setSprite(BufferedImage sprite) {
        this.sprite = sprite;
    }
 
    public State getState() {
        return state;
    }
 
    public void setState(State state) {
        this.state = state;
    }
 
    @Override
    public void render(Graphics2D graphics, int xOffset, int yOffset) {
        int renderX = getX() * 16 - xOffset, renderY = getY() * 16 - yOffset;
        if (color != null) {
            Color previousColor = graphics.getColor();
            graphics.setColor(color);
            graphics.fillRect(renderX, renderY, 16, 16);
            graphics.setColor(previousColor);
        } else {
            graphics.drawImage(getSprite(), renderX, renderY, null);
          //  graphics.fillRect(getTileBounds().x, getTileBounds().y, 16, 16);
        } 
    	//graphics.fillRect(getTileCollisionBounds().x * 16 - xOffset, getTileCollisionBounds().y * 16 - yOffset, getTileCollisionBounds().width, getTileCollisionBounds().height);
        
    }
 
    @Override
    public void tick() {
    	color = null;
    }
 
    /**
     * Constructor solely used to find other tiles in an array of tiles
     * based on their x and y values using our equals method.
     * @param x
     * @param y
     */
    public MapTile(int x, int y) {
        super(x, y, false, false, null);
    }
 
    /**
     * Draws a color of this tile. Make sure to call at the end of the render function
     * if you want to overwrite the tile sprite.
     * @param graphics
     * @param xOffset x offset passed from camera
     * @param yOffset y offset passed from camera
     * @param color color to draw this tile
     */
    public void drawColor(Graphics graphics, int xOffset, int yOffset, Color color) {
        Color previousColor = graphics.getColor();
        graphics.setColor(color);
        graphics.fillRect(getX(), getY(), 16, 16);
        graphics.setColor(previousColor);
    }
 
    public ArrayList<MapTile> getNeighbors() {    	
        return neighbors;
    }
 
    public void configureNeighbors(ArrayList<MapTile> tiles, int width, int height) {
    	int mapWidth = width - 1;
    	int mapHeight = height - 1;
        neighbors = new ArrayList<>();
        if(getX() == 0 && getY() == 0) {
            int indexOfBottomRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() + 1));
            int indexOfBottomTile = tiles.indexOf(new MapTile(getX(), getY() + 1));
            int indexOfRightTile = tiles.indexOf(new MapTile(getX() + 1, getY()));
            neighbors.add(tiles.get(indexOfBottomRightTile));
            neighbors.add(tiles.get(indexOfBottomTile));
            neighbors.add(tiles.get(indexOfRightTile));
        }
        if(getX() == 0 && getY() == mapWidth) {
            int indexOfTopRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() - 1));
            int indexOfTopTile = tiles.indexOf(new MapTile(getX(), getY() - 1));
            int indexOfRightTile = tiles.indexOf(new MapTile(getX() + 1, getY()));
            neighbors.add(tiles.get(indexOfTopRightTile));
            neighbors.add(tiles.get(indexOfTopTile));
            neighbors.add(tiles.get(indexOfRightTile));
        }
        if(getX() == mapWidth && getY() == mapWidth) {
            int indexOfTopLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY() - 1));
            int indexOfLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY()));
            int indexOfTopTile = tiles.indexOf(new MapTile(getX(), getY() - 1));
            neighbors.add(tiles.get(indexOfTopLeftTile));
            neighbors.add(tiles.get(indexOfLeftTile));
            neighbors.add(tiles.get(indexOfTopTile));
        }
        if(getX() == 0 && getY() > 0 && getY() < mapHeight) {
            int indexOfTopTile = tiles.indexOf(new MapTile(getX(), getY() - 1));
            int indexOfBottomTile = tiles.indexOf(new MapTile(getX(), getY() + 1));
            int indexOfTopRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() - 1));
            int indexOfRightTile = tiles.indexOf(new MapTile(getX() + 1, getY()));
            int indexOfBottomRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() + 1));
            neighbors.add(tiles.get(indexOfTopRightTile));
            neighbors.add(tiles.get(indexOfBottomRightTile));
            neighbors.add(tiles.get(indexOfRightTile));
            neighbors.add(tiles.get(indexOfBottomTile));
            neighbors.add(tiles.get(indexOfTopTile));
        }
        if(getX() == mapWidth && getY() > 0 && getY() != mapHeight) {
            int indexOfTopTile = tiles.indexOf(new MapTile(getX(), getY() - 1));
            int indexOfTopLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY() - 1));
            int indexOfLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY()));
            int indexOfBottomLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY() + 1));
            int indexOfBottomTile = tiles.indexOf(new MapTile(getX(), getY() + 1));
            neighbors.add(tiles.get(indexOfTopLeftTile));
            neighbors.add(tiles.get(indexOfBottomLeftTile));
            neighbors.add(tiles.get(indexOfLeftTile));
            neighbors.add(tiles.get(indexOfTopTile));
            neighbors.add(tiles.get(indexOfBottomTile));
        }
        if(getX() > 0 && getX() < mapWidth && getY() == 0) {
            int indexOfBottomTile = tiles.indexOf(new MapTile(getX(), getY() + 1));
            int indexOfLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY()));
            int indexOfRightTile = tiles.indexOf(new MapTile(getX() + 1, getY()));
            int indexOfBottomLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY() + 1));
            int indexOfBottomRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() + 1));
            neighbors.add(tiles.get(indexOfBottomRightTile));
            neighbors.add(tiles.get(indexOfBottomLeftTile));
            neighbors.add(tiles.get(indexOfRightTile));
            neighbors.add(tiles.get(indexOfLeftTile));
            neighbors.add(tiles.get(indexOfBottomTile));
        }
        if(getX() > 0 && getY() == mapHeight && getX() < mapWidth) {
            int indexOfTopTile = tiles.indexOf(new MapTile(getX(), getY() - 1));
            int indexOfLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY()));
            int indexOfRightTile = tiles.indexOf(new MapTile(getX() + 1, getY()));
            int indexOfTopLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY() - 1));
            int indexOfTopRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() - 1));
            neighbors.add(tiles.get(indexOfTopRightTile));
            neighbors.add(tiles.get(indexOfTopLeftTile));
            neighbors.add(tiles.get(indexOfRightTile));
            neighbors.add(tiles.get(indexOfTopTile));
            neighbors.add(tiles.get(indexOfLeftTile));
        }
        if(getX() > 0 && getX() < mapWidth && getY() > 0 && getY() < mapHeight) {
            int indexOfLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY()));
            int indexOfRightTile = tiles.indexOf(new MapTile(getX() + 1, getY()));
            int indexOfTopTile = tiles.indexOf(new MapTile(getX(), getY() - 1));
            int indexOfBottomTile = tiles.indexOf(new MapTile(getX(), getY() + 1));
            int indexOfTopLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY() - 1));
            int indexOfTopRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() - 1));
            int indexOfBottomLeftTile = tiles.indexOf(new MapTile(getX() - 1, getY() + 1));
            int indexOfBottomRightTile = tiles.indexOf(new MapTile(getX() + 1, getY() + 1));
            neighbors.add(tiles.get(indexOfBottomRightTile));
            neighbors.add(tiles.get(indexOfBottomLeftTile));
            neighbors.add(tiles.get(indexOfTopRightTile));
            neighbors.add(tiles.get(indexOfTopLeftTile));
            neighbors.add(tiles.get(indexOfBottomTile));
            neighbors.add(tiles.get(indexOfTopTile));
            neighbors.add(tiles.get(indexOfRightTile));
            neighbors.add(tiles.get(indexOfLeftTile));
        }
    }


}

