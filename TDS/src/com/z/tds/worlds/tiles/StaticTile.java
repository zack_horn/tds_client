package com.z.tds.worlds.tiles;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.z.tds.Client;

public class StaticTile extends MapTile {
	 
    private BufferedImage sprite;
 
    public StaticTile(int x, int y, boolean walkable, boolean isSolid, Client client, BufferedImage sprite) {
        super(x, y, walkable, isSolid, client);
        this.sprite = sprite;
    }
 
    public BufferedImage getSprite() {
        return sprite;
    }
 
    public void setSprite(BufferedImage sprite) {
        this.sprite = sprite;
    }
 
    public void render(Graphics2D graphics, int xOffset, int yOffset) {
        super.render(graphics, xOffset, yOffset);
    }
 
    @Override
    public void tick() {
        super.tick();
        switch (getState()) {
            case DEFAULT:
                setSprite(sprite);
                setColor(null);
                break;
            case SEARCHED:
                setSprite(null);
                setColor(Color.gray);
                break;
            case PATHED:
                setSprite(null);
                setColor(Color.red);
                break;
        }
        setState(State.DEFAULT);
    }
}
