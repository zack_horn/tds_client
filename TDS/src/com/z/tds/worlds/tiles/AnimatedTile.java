package com.z.tds.worlds.tiles;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.z.tds.Client;

public class AnimatedTile extends MapTile {
	 
    private ArrayList<BufferedImage> sprites;
    private int spriteIndex = 0;
 
    public AnimatedTile(int x, int y, boolean isWalkable, boolean isSolid, Client client, ArrayList<BufferedImage> sprites) {
        super(x, y, isWalkable, isSolid, client);
        this.sprites = sprites;
        if (sprites.size() > 0)
            setSprite(sprites.get(0));
    }
 
    @Override
    public void render(Graphics2D graphics, int xOffset, int yOffset) {
        super.render(graphics, xOffset, yOffset);
    }
 
    @Override
    public void tick() {
        super.tick();
        if (spriteIndex < sprites.size() - 1)
            spriteIndex++;
        else
            spriteIndex = 0;
        setSprite(sprites.get(spriteIndex));
    }
}
