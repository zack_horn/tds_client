package com.z.tds.worlds;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import com.z.tds.Client;
import com.z.tds.utilities.ImageResources;
import com.z.tds.worlds.tiles.AnimatedTile;
import com.z.tds.worlds.tiles.GrassTile;
import com.z.tds.worlds.tiles.MapTile;
import com.z.tds.worlds.tiles.RockTile;
import com.z.tds.worlds.tiles.StaticTile;
import com.z.tds.worlds.tiles.Tile;
import com.z.tds.worlds.tiles.WaterTile;

public class MapLoader {

	private Client client;
	private int mapHeight, mapWidth;
	private BufferedReader bufferedReader;

	public MapLoader(Client client) {
		this.client = client;
	}

	public ArrayList<MapTile> loadMap(String pathToMapFile) {
		ArrayList<MapTile> mapTiles = new ArrayList<MapTile>();
		InputStream mapInputStream = getClass().getResourceAsStream(
				pathToMapFile);
		bufferedReader = new BufferedReader(new InputStreamReader(
				mapInputStream));
		try {
			mapWidth = Integer.parseInt(bufferedReader.readLine().trim());
			mapHeight = Integer.parseInt(bufferedReader.readLine().trim());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int row = 0;
		String text = null;
		try {
			while ((text = bufferedReader.readLine()) != null) {
				String[] tileValues = text.split(",");
				for (int column = 0; column < mapWidth; column++) {
					int tileID = Integer.parseInt(tileValues[column]);
					if (tileID == 0) {
						// mapTiles.add(new StaticTile(column, row, true, false,
						// client,
						// imageResources.getStaticTileSprites().get(0)));
						mapTiles.add(new GrassTile(column, row, true, false, client, client.getImageResources().getRandomBasicGrassTileSprite()));
					}
					if (tileID == 1) {
						// mapTiles.add(new AnimatedTile(column, row, true,
						// false, client,
						// imageResources.getAnimatedTileSprites()));
						mapTiles.add(new WaterTile(column, row, true, false, client, client.getImageResources().getWaterTileAnimationSprites()));
					}
					if (tileID == 3) {
						// mapTiles.add(new StaticTile(column, row, false, true,
						// client, imageResources.getRockTileSprites().get(0)));
						mapTiles.add(new RockTile(column, row, false, true, client, client.getImageResources().getBasicRockTileSprite()));
					}
				}
				row++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		  for(Tile tile: mapTiles) { MapTile mapTile = (MapTile) tile;
		  mapTile.configureNeighbors(mapTiles, mapWidth, mapHeight); }
		 
		return mapTiles;
	}

	public int parseStringToInt(String string) {

		try {
			return Integer.parseInt(string);
		} catch (NumberFormatException e) {
			System.out.println("parse error on: " + string.length());
			e.printStackTrace();
			return 0;
		}
	}

	public String loadMapFileAsString() {
		StringBuilder builder = new StringBuilder();
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(
					"resources/map6x6.txt"));
			String line = "";
			try {
				while ((line = bufferedReader.readLine()) != null) {
					builder.append(line + getNewLine());
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				bufferedReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return builder.toString();
	}

	public String getNewLine() {
		return System.getProperty("line.separator");
	}

}