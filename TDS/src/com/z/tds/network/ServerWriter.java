package com.z.tds.network;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.SwingUtilities;

import com.z.tds.Client;
import com.z.tds.protocol.NetworkProtocol;

public class ServerWriter {
	
	
	private Client client;
	private Socket socket;
	private DataOutputStream dataOutputStream;
	
	public ServerWriter(Client client) {
		this.client = client;
	}
	
	public void init() {
		try {
			socket = new Socket("localhost", 5555);
		} catch (UnknownHostException e) {
			// Create alert of msg of error to user
			e.printStackTrace();
		} catch (IOException e) {
			// Create alert of msg of error to user
			e.printStackTrace();
		}
		try {
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			//  Create alert of msg of error to user
			e.printStackTrace();
		}
	}
	
	public Socket getSocket() {
		return this.socket;
	}
	
	public void sendPlayerPositionUpdate(String username, float xMovement, float yMovement) {
		
	} 
		
	public void sendCreateBulletToServer(String username, String bulletType, int originX, int originY, int destinationX, int destinationY) {
		String packetContents = NetworkProtocol.MAP_ITEM_PACKET_IDENTIFIER + NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER + NetworkProtocol.CREATE_BULLET + NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER +
		username + NetworkProtocol.END_USERNAME_IDENTIFIER + bulletType + NetworkProtocol.END_BULLET_TYPE_IDENTIFIER + originX + NetworkProtocol.FLOAT_SPLITTER_IDENTIFIER + originY + NetworkProtocol.END_ORIGIN_LOCATION_VALUE +
		destinationX + NetworkProtocol.FLOAT_SPLITTER_IDENTIFIER + destinationY;
		System.out.println(packetContents);
		
		//Below is how server should read to understand info
		String protocol = packetContents.substring(0, packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER));
		String subProtocol = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER));
		String usernameID = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_USERNAME_IDENTIFIER)).trim();
		String bulletTypeID = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_USERNAME_IDENTIFIER) + 3, packetContents.indexOf(NetworkProtocol.END_BULLET_TYPE_IDENTIFIER)).trim();
		String unConfigOriginXY = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_BULLET_TYPE_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_ORIGIN_LOCATION_VALUE) + 1).trim();
		String unConfigDestinationXY = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_ORIGIN_LOCATION_VALUE) + 2, packetContents.length()).trim();
		
		String originXY = unConfigOriginXY.replace('^', ' ');
		String destinationXY = unConfigDestinationXY.replace('^', ' ');
		
		String originXX = originXY.substring(0, originXY.indexOf("&")).trim();
		String originYY = originXY.substring(originXY.indexOf("&") + 1, originXY.length()).trim();
		
		System.out.println("protocol: " + protocol);
		System.out.println("subPprotocol: " + subProtocol);
		System.out.println("username: " + usernameID);
		System.out.println("bulletType: " + bulletTypeID);
		System.out.println("originXY: " + originXY);
		System.out.println("destinationXY: " + destinationXY);
		System.out.println("originXX: " + originXX);
		System.out.println("originYY: " + originYY);
	}
	
	public void sendLoginAttemptToServer(String username, String password) {
		
		//Below is configuration to send to server
		String packetContents = NetworkProtocol.LOGIN_PACKET_IDENTIFIER + NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER +
				NetworkProtocol.LOGIN_ATTEMPT + NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER + username + NetworkProtocol.END_USERNAME_IDENTIFIER + password;
		System.out.println(packetContents);
		
		//Below is how the server should read to understand info
		String protocol = packetContents.substring(0, packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER));
		String subProtocol = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER));
		String usernameID = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_USERNAME_IDENTIFIER)).trim();
		String passwordID = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_USERNAME_IDENTIFIER) + 3, packetContents.length()).trim();
		
		System.out.println("Main PROTOCOL: " + protocol);
		System.out.println("sub PROTOCOL: " + subProtocol);
		System.out.println("username: " + usernameID);
		System.out.println("pass:" + passwordID);
	}
	
	
	public void sendPacketAfterConfiguration(String packetContents) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				byte[] packet = packetContents.getBytes();
				int packetLengthToWrite = packet.length - (packet.length * 2);
				try {
					dataOutputStream.writeInt(packetLengthToWrite);
					dataOutputStream.write(packet, 0, packet.length);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

}
